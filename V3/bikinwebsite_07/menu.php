<!--<navbar menu>-->
<div class="bw_menu">
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container padding_lr10">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bw_floating_navmenu">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				
				<!--<logo>-->
				<div class="bw_logo_mobile">
					<a href="<?php echo home_url(); ?>"><img src="http://s31.postimg.org/lzs04qpt7/logo_Bikin_Website_Com.png"></a>
				</div>
				<!--</logo>-->
			</div>
			
				<?php
					wp_nav_menu( array(
						'menu'              => 'primary',
						'theme_location'    => 'primary',
						'depth'             => 2,
						'container'         => 'div',
						'container_class'   => 'navbar-collapse collapse',
						'container_id'      => 'bw_floating_navmenu',
						'menu_class'        => 'nav navbar-nav bw_navbar_center',
						'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
						'walker'            => new wp_bootstrap_navwalker())
					);
				?>
		</div>
	</nav>
</div>
<!--</navbar menu>-->